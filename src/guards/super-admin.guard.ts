import {
  CanActivate,
  ExecutionContext,
  Injectable,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Observable } from 'rxjs';

@Injectable()
export class SuperAdminGuard implements CanActivate {
  constructor(private jwtService: JwtService) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const req = context.switchToHttp().getRequest();
    try {
      const superAdminKey = req.headers['super-admin-header-key'];

      if (!superAdminKey) {
        throw new HttpException(
          'Forbidden. This path only for Super Admin',
          HttpStatus.FORBIDDEN,
        );
      }

      this.jwtService.verify(superAdminKey);
      return true;
    } catch (e) {
      throw new HttpException(
        'Forbidden. This path only for Super Admin',
        HttpStatus.FORBIDDEN,
      );
    }
  }
}
