import { Model, Table, Column, DataType, HasMany } from 'sequelize-typescript';
import { ApiProperty } from '@nestjs/swagger';

import { USER_ROLE } from '../../types/role';
import { Meme } from '../memes/memes.model';

interface UserCreationAttributes {
  email: string;
  password: string;
}

@Table({ tableName: 'users' })
export class User extends Model<User, UserCreationAttributes> {
  @ApiProperty({ example: 123, description: 'Uniqe ID' })
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @ApiProperty({ example: 'qweqwe@gmail.com', description: 'User email' })
  @Column({
    type: DataType.STRING,
    unique: true,
    allowNull: false,
  })
  email: string;

  @ApiProperty({ example: 'asd123', description: 'Password' })
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  password: string;

  @ApiProperty({ example: true })
  @Column({
    type: DataType.BOOLEAN,
    defaultValue: false,
  })
  banned: boolean;

  @ApiProperty({ example: 'Bad boy', description: 'Ban reason' })
  @Column({
    type: DataType.STRING,
    allowNull: true,
    defaultValue: null,
  })
  banReason: string;

  @ApiProperty({ example: 'User role', description: USER_ROLE.USER })
  @Column({
    type: DataType.STRING,
    allowNull: false,
    defaultValue: USER_ROLE.USER,
  })
  role: string;

  @HasMany(() => Meme)
  memes: Meme[];
}
