import { ApiProperty } from '@nestjs/swagger';
import { IsEnum } from 'class-validator';

import { USER_ROLE } from '../../../types/role';

export class UpdateUserRoleDto {
  @ApiProperty({ example: 'ADMIN', description: 'User role' })
  @IsEnum(USER_ROLE, { message: 'Incorect user role' })
  readonly role: USER_ROLE;
}
