import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Product, ProductDocument } from './schemas/product.schema';
import { CreateProductDto, UpdateProductDto } from './dto';

@Injectable()
export class ProductsService {
  constructor(
    @InjectModel(Product.name) private productModel: Model<ProductDocument>,
  ) {}

  async getAllProducts(): Promise<Product[]> {
    return this.productModel.find().exec();
  }

  async getProductById(id: string): Promise<Product> {
    return await this.productModel.findById(id);
  }

  async createProduct(body: CreateProductDto): Promise<Product> {
    const newProduct = new this.productModel(body);
    return newProduct.save();
  }

  async updateProduct(id: string, body: UpdateProductDto): Promise<Product> {
    return this.productModel.findByIdAndUpdate(id, body, { new: true });
  }

  async removeProduct(id: string): Promise<Product> {
    return this.productModel.findByIdAndRemove(id);
  }
}
