import {
  Controller,
  Get,
  Post,
  Body,
  UseGuards,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiSecurity,
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { JwtAuthGuard, UserReq, SuperAdminGuard } from '../../guards';
import { FileInterceptor } from '@nestjs/platform-express';

import { CreateMemeDto } from './dto';
import { Meme } from './memes.model';
import { MemesService } from './memes.service';

@ApiSecurity('super-admin')
@ApiBearerAuth()
@ApiTags('Meme route')
@UseGuards(JwtAuthGuard)
@UseGuards(SuperAdminGuard)
@Controller('memes')
export class MemesController {
  constructor(private readonly memeService: MemesService) {}

  @ApiOperation({ summary: 'Get all memes' })
  @ApiResponse({ status: 200, type: [Meme] })
  @Get()
  getAllUsers() {
    return this.memeService.getAllMemes();
  }

  @ApiOperation({ summary: 'Create meme' })
  @ApiResponse({ status: 200, type: Meme })
  @Post()
  @UseInterceptors(FileInterceptor('image'))
  createMeme(
    @Body() memeDto: CreateMemeDto,
    @UploadedFile() image: Express.Multer.File,
    @UserReq() user,
  ) {
    return this.memeService.createMeme(memeDto, image, user.id);
  }
}
