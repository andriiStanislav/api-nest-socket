import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';

import { FilesService } from '../files/files.service';
import { Meme } from './memes.model';
import { CreateMemeDto } from './dto';

@Injectable()
export class MemesService {
  constructor(
    @InjectModel(Meme) private memeRepository: typeof Meme,
    private fileService: FilesService,
  ) {}

  async getAllMemes() {
    const memes = await this.memeRepository.findAll({ include: { all: true } });
    return memes;
  }

  async createMeme(
    dto: CreateMemeDto,
    image: Express.Multer.File,
    userId: number,
  ) {
    const fileName = await this.fileService.createFile(image);
    const meme = await this.memeRepository.create({
      ...dto,
      userId,
      image: fileName,
    });
    return meme;
  }
}
