import { Module, forwardRef } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

import { AuthModule } from '../auth/auth.module';
import { FilesModule } from '../files/files.module';
import { User } from '../users/users.model';

import { MemesController } from './memes.controller';
import { MemesService } from './memes.service';
import { Meme } from './memes.model';

@Module({
  controllers: [MemesController],
  providers: [MemesService],
  imports: [
    SequelizeModule.forFeature([User, Meme]),
    forwardRef(() => AuthModule),
    FilesModule,
  ],
})
export class MemesModule {}
