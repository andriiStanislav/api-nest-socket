import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
// import { MongooseModule } from '@nestjs/mongoose';
import { SequelizeModule } from '@nestjs/sequelize';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

// module
import { UsersModule } from './modules/users/users.module';
import { AuthModule } from './modules/auth/auth.module';
import { MemesModule } from './modules/memes/memes.module';
import { FilesModule } from './modules/files/files.module';

// model
import { User } from './modules/users/users.model';
import { Meme } from './modules/memes/memes.model';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      // * you can use this example for run app in different environments
      // envFilePath: `.env.${process.env.NODE_ENV}`,
    }),
    SequelizeModule.forRoot({
      dialect: 'postgres',
      uri: process.env.PG_DB_URI,
      models: [User, Meme],
      autoLoadModels: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, 'static'),
    }),
    // TODO - remove MongooseModule. It is used in ProductsModule
    // MongooseModule.forRoot(process.env.MONGO_DB_URI),
    // TODO - remove ProductsModule all change to another module. Now it is test-enucation module
    // ProductsModule,
    UsersModule,
    AuthModule,
    MemesModule,
    FilesModule,
  ],
})
export class AppModule {}
